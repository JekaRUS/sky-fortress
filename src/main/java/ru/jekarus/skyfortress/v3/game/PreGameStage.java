package ru.jekarus.skyfortress.v3.game;

import ru.jekarus.skyfortress.v3.SkyFortressPlugin;
import ru.jekarus.skyfortress.v3.listener.LobbyListener;
import ru.jekarus.skyfortress.v3.scoreboard.SfScoreboards;

public class PreGameStage extends SfGameStage {

    private final SkyFortressPlugin plugin;

    private final LobbyListener lobbyListener;

    public PreGameStage(SkyFortressPlugin plugin)
    {
        this.plugin = plugin;

        this.lobbyListener = new LobbyListener(this.plugin);
    }

    @Override
    public void enable()
    {
        this.plugin.getScoreboards().setSideBar(SfScoreboards.Types.PRE_GAME);

        this.lobbyListener.register();
    }

    @Override
    public void disable()
    {
        this.lobbyListener.unregister();
    }

    @Override
    public SfGameStageType getType()
    {
        return SfGameStageType.PRE_GAME;
    }

}
