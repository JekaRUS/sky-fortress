package ru.jekarus.skyfortress.v3.listener;

import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.filter.Getter;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.world.World;
import ru.jekarus.skyfortress.v3.SkyFortressPlugin;
import ru.jekarus.skyfortress.v3.player.SfPlayer;
import ru.jekarus.skyfortress.v3.player.SfPlayers;

public class LobbyListener {

    private final SkyFortressPlugin plugin;
    private final SfPlayers players;

    public LobbyListener(SkyFortressPlugin plugin)
    {
        this.plugin = plugin;
        this.players = SfPlayers.getInstance();
    }

    public void register()
    {
        Sponge.getEventManager().registerListeners(this.plugin, this);
    }

    public void unregister()
    {
        Sponge.getEventManager().unregisterListeners(this);
    }

    @Listener
    public void onInteract(ChangeBlockEvent.Modify event, @First Player player)
    {
        SfPlayer sfPlayer = this.players.getOrCreatePlayer(player);
        for (Transaction<BlockSnapshot> transaction : event.getTransactions())
        {
            BlockSnapshot original = transaction.getOriginal();
            BlockType type = original.getState().getType();
            if (type.equals(BlockTypes.HEAVY_WEIGHTED_PRESSURE_PLATE))
            {
                this.plugin.getLobby().standOnPlate(player, sfPlayer, original);
            }
            else if (type.equals(BlockTypes.WOODEN_BUTTON))
            {
                this.plugin.getLobby().pressButton(player, sfPlayer, original);
            }
        }

    }

    @Listener
    public void onDisconnect(ClientConnectionEvent.Disconnect event, @Getter("getTargetEntity") Player player)
    {
        SfPlayer sfPlayer = this.players.getOrCreatePlayer(player);
        this.plugin.getLobby().playerDisconnect(sfPlayer, player);
    }

    @Listener
    public void onMove(MoveEntityEvent event, @Getter("getTargetEntity") Player player)
    {
        double playerY = event.getToTransform().getPosition().getY();
        double lobbyY = this.plugin.getLobby().getSettings().center.getPosition().getPosition().getY() - 25;

        if (playerY < lobbyY)
        {
            event.setToTransform(
                    new Transform<>(this.plugin.getLobby().getSettings().center.getPosition())
            );
        }

//        int fromY = event.getFromTransform().getLocation().getBlockY();
//        int toY = event.getToTransform().getLocation().getBlockY();
//
//        if (fromY < toY)
//        {
//            if (player.getName().equals("JekaRUS"))
//            {
//
//            }
//        }

    }

    @Listener
    public void onEntityDamage(DamageEntityEvent event, @Getter("getTargetEntity") Player player)
    {
        event.setCancelled(true);
    }

}
