package ru.jekarus.skyfortress.v3.listener;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.filter.Getter;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.scheduler.Task;
import ru.jekarus.skyfortress.v3.SkyFortressPlugin;
import ru.jekarus.skyfortress.v3.castle.SfCastle;
import ru.jekarus.skyfortress.v3.engine.CastleDeathEngine;
import ru.jekarus.skyfortress.v3.player.SfPlayer;
import ru.jekarus.skyfortress.v3.player.SfPlayers;
import ru.jekarus.skyfortress.v3.team.SfGameTeam;
import ru.jekarus.skyfortress.v3.team.SfTeam;
import ru.jekarus.skyfortress.v3.utils.SfUtils;

import java.util.Optional;

public class PlayerDeathListener {

    private final SkyFortressPlugin plugin;
    private final SfPlayers players;

    public PlayerDeathListener(SkyFortressPlugin plugin)
    {
        this.plugin = plugin;
        this.players = SfPlayers.getInstance();
    }

    public void register()
    {
        Sponge.getEventManager().registerListeners(this.plugin, this);
    }

    public void unregister()
    {
        Sponge.getEventManager().unregisterListeners(this);
    }

    @Listener
    public void onDeath(DestructEntityEvent.Death event, @Getter("getTargetEntity") Player player)
    {
        for (Player anotherPlayer : event.getCause().allOf(Player.class))
        {
            if (anotherPlayer != player)
            {
                anotherPlayer.getInventory().offer(ItemStack.of(ItemTypes.GOLD_INGOT, 1));
            }
        }

        Task.builder().delayTicks(1).execute(player::respawnPlayer).submit(this.plugin);
        this.checkPlayerLost(player);
    }

    private void checkPlayerLost(Player player)
    {
        Optional<SfPlayer> optionalSfPlayer = this.players.getPlayer(player);
        if (!optionalSfPlayer.isPresent())
        {
            return;
        }
        SfPlayer sfPlayer = optionalSfPlayer.get();
        SfTeam team = sfPlayer.getTeam();
        if (team.getType() != SfTeam.Type.GAME)
        {
            return;
        }
        SfGameTeam gameTeam = (SfGameTeam) team;
        SfCastle castle = gameTeam.getCastle();
        if (!castle.isCaptured())
        {
            return;
        }
        SfUtils.setPlayerSpectator(player);
        CastleDeathEngine.checkCapturedCastle(this.plugin, castle);
    }

}
