package ru.jekarus.skyfortress.v3.lobby;

import ru.jekarus.skyfortress.v3.utils.SfLocation;

public class SfLobbySettings {

    public boolean canJoin = false;
    public boolean canLeave = false;
    public boolean canReady = false;

    public SfLocation center;

}
